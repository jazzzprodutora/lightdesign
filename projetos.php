<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <span>Projetos</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Projetos</h2>
       </div>
    </section>
    <!-- Carrosel -->
    <div id="carrosel-destaque-interno" class="carousel slide carousel-fade carrosel-destaque" data-ride="carousel">
       <!-- Indicadores Carrosel-->
        <ol class="carousel-indicators container">
            <!-- item indicador -->
            <li data-target="#carrosel-destaque-interno" data-slide-to="0" class="active">
                <!-- Titulo indicador -->
                <h4>Título do Destaque</h4>
                <h5>Subtítulo</h5>
            </li>
            <!-- item indicador -->
            <li data-target="#carrosel-destaque-interno" data-slide-to="1">
                <!-- Titulo indicador -->
                <h4>Título do Destaque 2</h4>
                <h5>Subtítulo 2</h5>
            </li>
            <!-- item indicador -->
            <li data-target="#carrosel-destaque-interno" data-slide-to="2">
                <!-- Titulo indicador -->
                <h4>Título do Destaque 3</h4>
                <h5>Subtítulo 3</h5>
            </li>
        </ol>
        <div class="carousel-inner">
            <!-- Item carrosel -->
            <div class="carousel-item active">
                <!-- Imagem destaque -->
                <img class="d-block w-100" src="assets/imgs/Smart-Fit-Santana.png" alt="Praesent sapien massa, convallis a pellentesque nec, engestas non nisi." title="Praesent sapien massa, convallis a pellentesque nec, engestas non nisi.">
                <!-- Conteudo carrosel -->
                <div class="caption-carrosel container">
                    <h2>Praesent sapien massa, convallis a pellentesque nec, engestas non nisi.</h2>
                    <div class="btn-cta">
                       <a href="projeto-destaque.php">Call to Action <img class="icon-right" src="assets/icons/arrow-white-right.svg" alt="Saiba mais"></a>
                    </div>
                </div>
            </div>
            <!-- Item carrosel -->
            <div class="carousel-item">
                <!-- Imagem destaque -->
                <img class="d-block w-100" src="assets/imgs/banner-02.png" alt="Praesent sapien massa, convallis a pellentesque nec, engestas non nisi." title="Praesent sapien massa, convallis a pellentesque nec, engestas non nisi.">
                <!-- Conteudo carrosel -->
                <div class="caption-carrosel container">
                    <h2>Engestas non Praesent sapien massa, </h2>
                    <div class="btn-cta">
                       <a href="projeto-destaque.php">Call to Action <img class="icon-right" src="assets/icons/arrow-white-right.svg" alt="Saiba mais"></a>
                    </div>
                </div>
            </div>
            <!-- Item carrosel -->
            <div class="carousel-item">
                <!-- Imagem destaque -->
                <img class="d-block w-100" src="assets/imgs/banner-03.png" alt="Praesent sapien massa, convallis a pellentesque nec, engestas non nisi." title="Praesent sapien massa, convallis a pellentesque nec, engestas non nisi.">
                <!-- Conteudo carrosel -->
                <div class="caption-carrosel container">
                    <h2>Convallis a pellentesque nec, engestas non nisi Praesent sapien massa.</h2>
                    <div class="btn-cta">
                       <a href="projeto-destaque.php">Call to Action <img class="icon-right" src="assets/icons/arrow-white-right.svg" alt="Saiba mais"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="lista-projetos">
        <div class="container">
            <nav class="navegacao-projetos">
                <ul class="list-unstyled">
                    <li class="ativo"><a href="">Todos</a></li>
                    <li><a href="">Residencial</a></li>
                    <li><a href="">Comercial</a></li>
                    <li><a href="">Jardim</a></li>
                    <li><a href="">Corporativo</a></li>
                    <li><a href="">Público</a></li>
                </ul>
            </nav>
        </div>
        <div class="container">
            <div class="row content-projeto">
                <article class="col-md-6 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/bhotel.png" alt="Hotel B">
                        </div>
                        <h3>Hotel B</h3>
                    </a>
                </article>
                <article class="col-md-6 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/Smart-Fit-Santana.png" alt="Smart Fit">
                        </div>
                        <h3>Smart Fit</h3>
                    </a>
                </article>
            </div>
            <div class="row content-projeto">
                <article class="col-md-8 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/bhotel.png" alt="Hotel B">
                        </div>
                        <h3>Smart Fit</h3>
                    </a>
                </article>
                <article class="col-md-4 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/Smart-Fit-Santana.png" alt="Smart Fit">
                        </div>
                        <h3>Hotel B</h3>
                    </a>
                </article>
            </div>
            <div class="row content-projeto">
                <article class="col-md-6 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/bhotel.png" alt="Hotel B">
                        </div>
                        <h3>Hotel B</h3>
                    </a>
                </article>
                <article class="col-md-6 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/Smart-Fit-Santana.png" alt="Smart Fit">
                        </div>
                        <h3>Smart Fit</h3>
                    </a>
                </article>
            </div>
            <div class="row content-projeto">
                <article class="col-md-8 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/bhotel.png" alt="Hotel B">
                        </div>
                        <h3>Smart Fit</h3>
                    </a>
                </article>
                <article class="col-md-4 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/Smart-Fit-Santana.png" alt="Smart Fit">
                        </div>
                        <h3>Hotel B</h3>
                    </a>
                </article>
            </div>
        </div>
    </section>
<!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>
