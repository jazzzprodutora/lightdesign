<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <a href="produtos.php">Produtos</a>
            <a href="produtos-area.php">Interior</a>
            <a href="produtos-categoria.php">Pendentes</a>
            <span>Linha Works</span>
        </div>
    </section>
    <!-- Chamada produto destaque -->
    <section class="chamada-destaque">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Works</h1>
                    <p>Embutidos com tecnologia LED integrado para uso interno. Corpo em alumínio e acabamento em pintura pó epóxi por processo eletroestático.</p>
                    <p>Refletor integrado de alto rendimento, disponível com diferentes acabamentos. Driver incluído.</p>
                </div>
                <div class="col-md-6">
                    <div class="owl-carousel wol-theme carrosel-produto-destaque">
                        <div class="item">
                            <div class="img-destaque">
                                <img src="assets/imgs/produtos/produto-destaque.png" alt="Works Move">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-destaque">
                                <img src="assets/imgs/produtos/produto-destaque.png" alt="Works Move">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-destaque">
                                <img src="assets/imgs/produtos/produto-destaque.png" alt="Works Move">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Vídeo -->
    <section class="video-destaque container mt-5">
        <video width="100%" controls>
			<source src="assets/video/video.mp4" type="video/mp4">
		</video>
    </section>
    <!-- Características Técnicas -->
    <section class="caracteristicas">
        <div class="container">
           <div class="row">
               <div class="col-md-5">
                   <h2>Características Técnicas</h2>
                   <ul class="list-unstyled list-caracteristicas">
                       <li>Elevado conforto visual</li>
                       <li>Lentes de precisão italianas</li>
                       <li>Fácil instalação</li>
                       <li>Modelos frame, deep e move</li>
                       <li>Refletores com dois níveis de recuo</li>
                       <li>Excelentes reprodução de cores</li>
                   </ul>
                   <ul class="list-unstyled selos">
                        <li><img src="assets/icons/selo-1.png" alt=""></li>
                        <li><img src="assets/icons/selo-2.png" alt=""></li>
                        <li><img src="assets/icons/selo-3.png" alt=""></li>
                        <li><img src="assets/icons/selo-4.png" alt=""></li>
                   </ul>
               </div>
           </div>
        </div>
    </section>
    <section class="modelos">
        <div class="container">
            <h2>Modelos</h2>
            <!-- Acordion modelo -->
            <div class="acoordions-modelos">
                <div class="accordions">
                    <button class="accordion-modelo">
                        <div class="accordion-titulos">
                            <div class="img-titulo">
                                <img src="assets/imgs/produtos/produto-destaque.png" alt="">
                            </div>
                            <h3>Works Frame</h3>
                        </div>
                        <div class="icons">
                            <!-- Icone + -->
                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus">
                            <!-- Icone - -->
                            <img class="minus-icon" src="assets/icons/minus-icon.svg" alt="minus"> 
                            <a href="" class="link-download" download><img class="download-icon" src="assets/icons/download.svg" alt="download">  </a>           
                        </div>
                    </button>
                    <!-- Conteudo -->
                    <div class="panel">
                        <!-- Tabela Modelos -->
                        <div class="table-responsive-lg">
                            <table class="table">
                                <!-- HEADER DA TABELA -->
                                <thead>
                                    <tr>
                                        <!-- Colunas HEader -->
                                        <th scope="col">Tamanho</th>
                                        <th scope="col">K</th>
                                        <th scope="col">Im</th>
                                        <th scope="col">Ângulo</th>
                                        <th scope="col">W</th>
                                        <th scope="col">Código</th>
                                        <th scope="col">Detalhes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Linha 1 -->
                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                        <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>

                                    </tr>
                                    <!-- Linha 2 -->

                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                        <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>

                                    </tr>
                                    <!-- Linha 3 -->
                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                        <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>   
                </div>
            </div>
            <!-- Acordion modelo -->
            <div class="acoordions-modelos">
                <div class="accordions">
                    <button class="accordion-modelo">
                        <div class="accordion-titulos">
                            <div class="img-titulo">
                                <img src="assets/imgs/produtos/produto-destaque.png" alt="">
                            </div>
                            <h3>Works Deep</h3>
                        </div>
                        <div class="icons">
                            <!-- Icone + -->
                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus">
                            <!-- Icone - -->
                            <img class="minus-icon" src="assets/icons/minus-icon.svg" alt="minus"> 
                            <!-- Download -->
                            <a href="" class="link-download" download><img class="download-icon" src="assets/icons/download.svg" alt="download">  </a>

                        </div>
                    </button>
                    <!-- Conteudo -->
                    <div class="panel">
                        <!-- Tabela Modelos -->
                        <div class="table-responsive-lg">
                            <table class="table">
                                <!-- HEADER DA TABELA -->
                                <thead>
                                    <tr>
                                        <!-- Colunas HEader -->
                                        <th scope="col">Tamanho</th>
                                        <th scope="col">K</th>
                                        <th scope="col">Im</th>
                                        <th scope="col">Ângulo</th>
                                        <th scope="col">W</th>
                                        <th scope="col">Código</th>
                                        <th scope="col">Detalhes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Linha 1 -->
                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                       <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>

                                    </tr>
                                    <!-- Linha 2 -->

                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                       <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>

                                    </tr>
                                    <!-- Linha 3 -->
                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                       <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>   
                </div>
            </div>
            <!-- Accordion modelo -->
            <div class="acoordions-modelos">
                <div class="accordions">
                    <button class="accordion-modelo">
                        <div class="accordion-titulos">
                            <div class="img-titulo">
                                <img src="assets/imgs/produtos/produto-destaque.png" alt="">
                            </div>
                            <h3>Works Moove</h3>
                        </div>
                        <div class="icons">
                            <!-- Icone + -->
                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus">
                            <!-- Icone - -->
                            <img class="minus-icon" src="assets/icons/minus-icon.svg" alt="minus"> 
                            <!-- download -->
                            <a href="" class="link-download" download><img class="download-icon" src="assets/icons/download.svg" alt="download">  </a>

                        </div>
                    </button>
                    <!-- Conteudo -->
                    <div class="panel">
                        <!-- Tabela Modelos -->
                        <div class="table-responsive-lg">
                            <table class="table">
                                <!-- HEADER DA TABELA -->
                                <thead>
                                    <tr>
                                        <!-- Colunas HEader -->
                                        <th scope="col">Tamanho</th>
                                        <th scope="col">K</th>
                                        <th scope="col">Im</th>
                                        <th scope="col">Ângulo</th>
                                        <th scope="col">W</th>
                                        <th scope="col">Código</th>
                                        <th scope="col">Detalhes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Linha 1 -->
                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                       <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                                           </td>

                                    </tr>
                                    <!-- Linha 2 -->

                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td>
                                       <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>

                                    </tr>
                                    <!-- Linha 3 -->
                                    <tr>
                                        <th scope="row">θ 93mm</th>
                                        <td>2700</td>
                                        <td>2375</td>
                                        <td>13º</td>
                                        <td>17</td>
                                        <td>820.0113</td> 
                                       <td >
                                            <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus" data-toggle="modal" data-target="#moda-destalhes">
                                                       
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>
    <!-- Projetos -->
    <section class="projetos-lista">
        <div class="container">
            <h2>Galeria</h2>
            <div class="row">
                <div class="col-md-3">
                    <!-- Lightbox -->
                   <a class="light-box-item" href="assets/imgs/projeto-2.png" data-lightbox="example-set" data-title="">
                        <img class="example-image img-destacada" src="assets/imgs/projeto-2.png" title="" alt=""/>
                    </a>
                </div>
                <div class="col-md-6">
                    <!-- Lightbox -->
                    <a class="light-box-item" href="assets/imgs/projeto-1.png" data-lightbox="example-set" data-title="">
                        <img class="example-image img-destacada" src="assets/imgs/projeto-1.png" title="" alt=""/>
                    </a>
                </div>
                <div class="col-md-3">
                    <!-- Lightbox -->
                   <a class="light-box-item" href="assets/imgs/projeto-3.png" data-lightbox="example-set" data-title="">
                        <img class="example-image img-destacada" src="assets/imgs/projeto-3.png" title="" alt=""/>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Produtos relacionados -->
    <section class="produtos-relacionados">
        <div class="container">
            <h2>Relacionados</h2>
        </div>
        <!-- Lista de produtos -->
        <div class="container">
            <div class="row">
                <article class="col-md-3 item-recente">
                    <a href="">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-1.png" alt="Downlight">
                        </div>
                        <h4>Nome do Produto</h4>
                    </a>
                </article>
                <article class="col-md-3 item-recente">
                    <a href="">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-2.png" alt="Geral">
                        </div>
                        <h4>Nome do Produto</h4>
                    </a>
                </article>
                <article class="col-md-3 item-recente">
                    <a href="">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-3.png" alt="Spots/Trilhos">
                        </div>
                        <h4>Nome do Produto</h4>
                    </a>
                </article>
                <article class="col-md-3 item-recente">
                    <a href="">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-4.png" alt="Lineares">
                        </div>
                        <h4>Nome do Produto</h4>
                    </a>
                </article>
                
            </div>
        </div>
    </section>
    <!-- Modal -->
    <?php require_once('modal.php');?>

    <!-- Rodapé -->
    <?php require_once('footer.php');?>

</main>

