<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="">Home</a>
            <a href="">Novidades</a>
            <span>Título novidade</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Novidades</h2>
       </div>
    </section>
    <!-- Seção notícia destaque -->
    <section class="novidade-destaque container">
        <div class="row justify-content-end">
            <!-- Notícia -->
            <article class="col-md-8">
                <div class="info-novidade">
                    <!-- Data -->
                    <span class="data-novidade">22/05/2021</span>
                    <!-- Título -->
                    <div class="titulo-novidade">
                        <h3>Proin Eget Tortor Risus Vestibulum Tortor Risus roineget  Vestibulum</h3>
                    </div>
                    <!-- Categoria -->
                    <div class="cat">
                        <a href="">Eventos</a>
                    </div>
                </div>
                <!-- Imagem destaque -->
                <div class="img-destaque">
                    <img src="assets/imgs/novidades/img-novidade.jpg" alt="Descrição imagem">
                </div>
                <div class="content">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum velit nostrum praesentium delectus consequatur possimus, dolor corporis autem, iure numquam rerum consequuntur eaque enim dolorem provident quis ex odio accusantium?</p>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam minima saepe delectus, officia accusantium inventore suscipit ex numquam nam voluptates officiis dolor quasi facilis veritatis incidunt ipsa, consectetur maxime. Suscipit. Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae ipsum consequuntur tempore voluptas amet laboriosam vel nihil commodi soluta. Quisquam quidem illum ad impedit debitis quae, id fugiat temporibus provident? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolores labore exercitationem quae impedit iusto harum quasi corporis esse recusandae, rerum ex consequatur, hic totam accusamus aliquam et officiis vel perspiciatis?</p>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae explicabo vero pariatur modi similique. Tempore, dolorem magni nulla veniam dolor ad qui error nesciunt harum fuga aliquid pariatur esse totam.</p>
                </div>
            </article>
        </div>
        <div class="row justify-content-end">
            <div class="col-md-8">
                <aside class="outras-novidades">
                    <a href=""><img class="arrow-left" src="assets/icons/arrow-left.svg" alt="Anterior">Anterior</a>
                    <a href="">Próxima <img class="arrow-right" src="assets/icons/arrow-right.svg" alt="Próxima"></a>
                </aside>
            </div>
        </div>
    </section>
 <!-- Rodapé -->
 <?php require_once('footer.php');?>
</main>