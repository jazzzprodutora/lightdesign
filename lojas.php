<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
      <!-- BreadCrumbs (Migalha de pão) -->
      <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <span>Pontos de Venda</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Pontos de Venda</h2>
       </div>
    </section>
    <!-- Mapa das lojas -->
    <section class="mapa-lojas">
        <div class="container">
            <img src="assets/imgs/mapa.png" alt="">
        </div>
    </section>
    <!-- Locais -->
    <section class="acoordions-local container">
        <div class="accordions">
            <button class="accordion-lojas">Showrooms <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus"><img class="minus-icon" src="assets/icons/minus-icon.svg" alt="minus"></button>
            <!-- Conteudo -->
            <div class="panel">
                <div class="row justify-content-end">
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                             </ul>
                        </address>
                    </div>
                 </div>
                 <div class="row justify-content-end">
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                             </ul>
                        </address>
                    </div>
                 </div>
            </div>
        </div>
    </section>
    <!-- Acoordeon -->
    <section class="acoordions-local container">
        <div class="accordions">
            <button class="accordion-lojas">Revenda <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus"><img class="minus-icon" src="assets/icons/minus-icon.svg" alt="minus"></button>
            <!-- Conteudo -->
            <div class="panel">
                <div class="row justify-content-end">
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                             </ul>
                        </address>
                    </div>
                 </div>
                 <div class="row justify-content-end">
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                             </ul>
                        </address>
                    </div>
                 </div>
            </div>
        </div>
    </section>
        <!-- Acoordeon -->
        <section class="acoordions-local container">
        <div class="accordions">
            <button class="accordion-lojas">Atendimento Corporativo <img class="plus-icon" src="assets/icons/plus-icon.svg" alt="plus"><img class="minus-icon" src="assets/icons/minus-icon.svg" alt="minus"></button>
            <!-- Conteudo -->
            <div class="panel">
                <div class="row justify-content-end">
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                             </ul>
                        </address>
                    </div>
                 </div>
                 <div class="row justify-content-end">
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                            </ul>
                        </address>
                    </div>
                    <div class="col-lg-3">
                        <address>
                            <ul class="list-unstyled">
                                <li>Brasilia</li>
                                <li>Mercado Design</li>
                                <li>SIA Trecho 3 lotes 165/195</li>
                                <li>loja 101 CEP 71200 030 - DF</li>
                                <li><a href="mailto:contato@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Email"> Email</a> <a href=""><img class="arrow-right" src="assets/icons/arrow-right.svg" title="Google Maps">  Google Maps</a></li>
                             </ul>
                        </address>
                    </div>
                 </div>
            </div>
        </div>
    </section>

  
<!-- Rodapé -->
</main>
<?php require_once('footer.php');?>
