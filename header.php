<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/icons/fav.png">
        <!-- Bootstrap-->
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="assets/css/lightbox.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">

        <link rel="stylesheet" href="assets/css/main.css">
        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/bootstrap.bundle.js"></script>
        <script src="assets/js/lightbox.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <!--[if lt IE 7]>
                    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
        <![endif]-->
        <title>Lightdesign | Home</title>
    </head>
    <body>
    <!-- Header  desktop-->
    <header class="header-site">    
        <div class="container cont-header">
            <div class="row">
                <div class="col-sm-4  menu-busca">
                    <div class="menu-icon">
                        <div class="hamburger-menu">
                            <div id="linha1"></div>
                            <div id="linha2"></div>
                         
                        </div>
                        <span class="legenda-menu">Menu</span>
                    </div>
                    <div class="menu-desk">
                        <a href=""><img src="assets/icons/search-icon.svg" alt="Busca"> Busca</a>
                    </div>
                </div>
                <div class="col-sm-4  brand">
                    <a href="index.php"><img src="assets/icons/logo.png" alt="Lightdesign + exporlux"></a>
                </div>
                <div class="col-lg-4 lang-login">
                    <div class="lang">
                        <a href="" class="ativo">Pt</a>
                        <a href="" >En</a>
                    </div>
                    <div class="login">
                        <a href=""><img src="assets/icons/arrow-right.svg" alt=""> Login</a>
                    </div>
                </div>
            </div>
        </div> 
    </header>
    <!-- Menu  -->
    <div id="myNav" class="overlay">
        <nav class="overlay-content">
            <div class="container">
                <!-- Menu -->
                <section class="acoordions-header">
                    <div class="accordions">
                        <button class="accordion-menu">Produtos <div class="plus"><img class="branco-plus" src="assets/icons/plus-white-icon.svg" alt=""><img class="verde-plus" src="assets/icons/plus-verde-icon.svg" alt=""></div><div class="minus"><img class="branco-minus" src="assets/icons/minus-white-icon.svg" alt=""><img class="verde-minus" src="assets/icons/minus-verde-icon.svg" alt=""></div></button>
                        <!-- Conteudo -->
                        <div class="panel">
                            <ul class="list-unstyled">
                                <li><a href="produtos-area.php">Interior</a></li>
                                <li><a href="produtos-area.php">Exterior</a></li>
                                <li><a href="produtos-area.php">Lançamentos</a></li>
                            </ul>
                        </div>   
                    </div>
                </section>
                <section class="acoordions-header">
                    <div class="accordions">
                        <button class="accordion-menu">Projetos <div class="plus"><img class="branco-plus" src="assets/icons/plus-white-icon.svg" alt=""><img class="verde-plus" src="assets/icons/plus-verde-icon.svg" alt=""></div><div class="minus"><img class="branco-minus" src="assets/icons/minus-white-icon.svg" alt=""><img class="verde-minus" src="assets/icons/minus-verde-icon.svg" alt=""></div></button>
                        <!-- Conteudo -->
                        <div class="panel">
                            <ul class="list-unstyled">
                                <li><a href="projetos.php">Comercial</a></li>
                                <li><a href="projetos.php">Residencial</a></li>
                                <li><a href="projetos.php">Corporativo</a></li>
                                <li><a href="projetos.php">Outros</a></li>

                            </ul>
                        </div>   
                    </div>
                </section>  
                <section class="menu-list">
                    <ul class="list-unstyled">
                        <li><a href="novidades.php">Novidades</a></li>
                        <li><a href="lojas.php">Pontos de Venda</a></li>
                        <li><a href="sobre.php">Sobre</a></li>
                        <li><a href="contato.php">Contato</a></li>
                    </ul>
                </section>
                <section class="mobile-search">
                    <a href=""><img class="mr-2" src="assets/icons/search-white-icon.svg" alt=""> Busca</a>
                </section>
                <section class="mobile-lang">
                    <ul class="list-unstyled">
                        <li><a href="" class="ativo">Pt</a> <a href="" >En</a></li>
                        <li><a href="" class="login"><img class="mr-2" src="assets/icons/arrow-white-right.svg" alt=""> Login</a></li>
                    </ul>
                </section>
            </div>
        </nav>
    </div>
    
