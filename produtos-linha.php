<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="">Home</a>
            <a href="produtos.php">Produtos</a>
            <a href="produtos-area.php">Interior</a>
            <a href="produtos-categoria.php">Pendentes</a>
            <span>Linhas</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Linhas</h2>
       </div>
    </section>
    <section class="video-destaque container">
        <video width="100%" controls>
			<source src="assets/video/video.mp4" type="video/mp4">
		</video>
    </section>
    <!-- Outros produtos -->
    <section class="produtos-categoria">
        <div class="container">
            
            <!-- Navegação de categorias -->
            <nav class="navegacao-categoria">
                <ul class="list-unstyled">
                    <!-- Dropdown item -->
                    <li>
                        <div class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Aplicação <i class="fas fa-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Categoria </a>
                                <a class="dropdown-item" href="#">Outra Categoria</a>
                                <a class="dropdown-item" href="#">Nova Categoria</a>
                            </div>
                        </div>
                    </li>
                    <!-- Dropdown item -->
                    <li>
                        <div class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Distribuição de Luz <i class="fas fa-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Categoria </i></a>
                                <a class="dropdown-item" href="#">Outra Categoria </i></a>
                                <a class="dropdown-item" href="#">Nova Categoria </i></a>
                            </div>
                        </div>
                    </li>                
                </ul>
            </nav>
        </div>
        <!-- Lista de produtos -->
        <div class="cont-lista-produtos container">
            <article>
               <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-1.png" alt="Downlight">
                    </div>
                    <h4>Downlight</h4>
               </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-2.png" alt="Geral">
                    </div>
                    <h4>Geral</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-3.png" alt="Spots/Trilhos">
                    </div>
                    <h4>Spots/Trilhos</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-4.png" alt="Lineares">
                    </div>
                    <h4>Lineares</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-5.png" alt="Modulares">
                    </div>
                    <h4>Modulares</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-6.png" alt="Perfis">
                    </div>
                    <h4>Perfis</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-1.png" alt="Downlight">
                    </div>
                    <h4>Downlight</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-2.png" alt="Geral">
                    </div>
                    <h4>Geral</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-3.png" alt="Spots/Trilhos">
                    </div>
                    <h4>Spots/Trilhos</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-4.png" alt="Lineares">
                    </div>
                    <h4>Lineares</h4>
                </a>
            </article>
        </div>
    </section>
    <section class="visto-recente">
        <div class="container">
            <h2>Visualizados Recentemente</h2>
        </div>
        <!-- Lista de produtos -->
        <div class="container">
            <div class="row">
                <article class="col-md-3 item-recente">
                    <a href="produto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-1.png" alt="Downlight">
                        </div>
                        <h4>Nome do Produto</h4>
                    </a>
                </article>
            <article class="col-md-3 item-recente">
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-2.png" alt="Geral">
                    </div>
                    <h4>Nome do Produto</h4>
                </a>
            </article>
            <article class="col-md-3 item-recente">
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-3.png" alt="Spots/Trilhos">
                    </div>
                    <h4>Nome do Produto</h4>
                </a>
            </article>
            <article class="col-md-3 item-recente">
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-4.png" alt="Lineares">
                    </div>
                    <h4>Nome do Produto</h4>
                </a>
            </article>
            </div>
        </div>
    </section>
    <!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>

