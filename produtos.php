<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <span>Produtos</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Produtos</h2>
       </div>
    </section>
    <!-- Tipos de produtos -->
    <section class="tipo-produtos-destaque">
        <div class="container">
            <div class="row">
                <div class="col-md-6 tipo-produto">
                    <a href="produtos-area.php">
                        <div class="img-content">
                            <img src="assets/imgs/interna.jpg" alt="Interior" title="Interior">
                        </div> 
                        <div class="titulo-produto">
                            <h3>Interior</h3>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 tipo-produto">
                    <a href="produtos-exterior.php">
                        <div class="img-content">
                            <img src="assets/imgs/externa.jpg" alt="Exterior" title="Exterior">
                        </div> 
                        <div class="titulo-produto">
                            <h3>Exterior</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Lançamentos -->
    <section class="produto-lancamento">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <!-- Coluna Informação -->
                <div class="col-md-6 align-self-center justify-content-center info-lancamento">
                    <div class="info">
                        <h3>Lançamentos</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt, illum!onsectetur adipisicing elit .</p>
                        <div class="btn-cta">
                            <a href="produtos-lancamentos.php">Saiba mais <img class="arrow-right" src="assets/icons/arrow-right.svg" alt=""></a>
                        </div>
                    </div>
                </div>
                <!-- Coluna Imagem -->
                <div class="col-md-6 align-self-center img-lancamento">
                    <a href="produtos-lancamentos.php">
                        <img src="assets/imgs/lancamento.jpg" alt="Lançamentos" title="lançamentos">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>

