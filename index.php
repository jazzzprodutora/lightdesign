<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<!-- Botão scroll top home -->
<aside class="scroll-top-home">
    <a href="#"><img src="assets/icons/arrow-top-white.svg" alt="Scrool Top" title="Top"></a>
</aside>
<main class="home">
    <!-- Carrosel -->
    <div id="carrosel-destaque-home" class="carousel slide carousel-fade carrosel-destaque" data-ride="carousel">
       <!-- Indicadores Carrosel-->
        <ol class="carousel-indicators container">
            <!-- item indicador -->
            <li data-target="#carrosel-destaque-home" data-slide-to="0" class="active">
                <!-- Titulo indicador -->
                <h4>Design, inovação e tecnologia em iluminação desde 1974.</h4>
                <h5>Subtítulo</h5>
            </li>
            <!-- item indicador -->
            <li data-target="#carrosel-destaque-home" data-slide-to="1">
                <!-- Titulo indicador -->
                <h4>Design, inovação e tecnologia em iluminação desde 1974. 2</h4>
                <h5>Subtítulo 2</h5>
            </li>
            <!-- item indicador -->
            <li data-target="#carrosel-destaque-home" data-slide-to="2">
                <!-- Titulo indicador -->
                <h4>Design, inovação e tecnologia em iluminação desde 1974. 3</h4>
                <h5>Subtítulo 3</h5>
            </li>
        </ol>
        <div class="carousel-inner">
            <!-- Item carrosel -->
            <div class="carousel-item active">
                <!-- Imagem destaque -->
                <img class="d-block w-100" src="assets/imgs/Smart-Fit-Santana.png" alt="First slide">
                <!-- Conteudo carrosel -->
                <div class="caption-carrosel container">
                    <h2>Design, inovação e tecnologia em iluminação desde 1974.</h2>
                    <div class="btn-cta">
                        <a href="projeto-destaque.php">Call to Action <img class="icon-right" src="assets/icons/arrow-white-right.svg" alt=""></a>
                    </div>
                </div>
            </div>
            <!-- Item carrosel -->
            <div class="carousel-item">
                <!-- Imagem destaque -->
                <img class="d-block w-100" src="assets/imgs/banner-02.png" alt="First slide">
                <!-- Conteudo carrosel -->
                <div class="caption-carrosel container">
                    <h2>Design, inovação e tecnologia em iluminação desde 1974.</h2>
                    <div class="btn-cta">
                        <a href="projeto-destaque.php">Call to Action <img class="icon-right" src="assets/icons/arrow-white-right.svg" alt=""></a>
                    </div>
                </div>
            </div>
            <!-- Item carrosel -->
            <div class="carousel-item">
                <!-- Imagem destaque -->
                <img class="d-block w-100" src="assets/imgs/banner-03.png" alt="First slide">
                <!-- Conteudo carrosel -->
                <div class="caption-carrosel container">
                    <h2>Design, inovação e tecnologia em iluminação desde 1974.</h2>
                    <div class="btn-cta">
                        <a href="projeto-destaque.php">Call to Action <img class="icon-right" src="assets/icons/arrow-white-right.svg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Conteúdo textual -->
    <section class="bloco-texto">
       <div class="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error debitis nostrum quae explicabo quia, id non rerum beatae! Expedita debitis veritatis repudiandae porro quaerat quod minus iure repellat officiis fugit.</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Excepturi voluptatum provident, eaque ex repellendus maxime odio accusamus quod, dicta iste tempora dignissimos officiis tenetur illo fuga veritatis nostrum dolorem! Beatae.</p>
            <a href="sobre.php"><img class="icon-right" src="assets/icons/arrow-white-right.svg" alt="">Quem Somos</a>
       </div>
    </section>
<!-- Rodapé -->
    <?php require_once('footer-home.php');?>
</main>
