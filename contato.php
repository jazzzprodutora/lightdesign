<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="">Home</a>
            <span>Contato</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Contato</h2>
       </div>
    </section>
    <!-- Unidades -->
    <section class="cont-unidades container">
        <div class="row">
            <!-- Coluna unidades -->
            <div class="col-md-6 address">
                <h4>Unidade Fabril</h4>
                <div class="cont-adress">
                    <!-- Unidade BR -->
                    <address>
                        <ul class="list-unstyled">
                            <li><h5>Brasil</h5></li>
                            <li><a href="tel:+55581 3030-4040">+55581 3030-4040</a></li>
                            <li><a class="email" href="mailto:contato@lighdesign.com.br">contato@lighdesign.com.br</a></li>
                        </ul>
                    </address>
                    <!-- Unidade PT -->
                    <address>
                        <ul class="list-unstyled">
                            <li><h5>Portugal</h5></li>
                            <li>Rua Logradouro, 1532</li>
                            <li>Nome do Bairro</li>
                            <li>Lisboa Portugal</li>
                            <li><a class="email" href="mailto:contato@lighdesign.com.br">contato@lighdesign.com.br</a></li>
                        </ul>
                    </address>
                </div>
            </div>
            <!-- Coluna Grupo -->
            <div class="col-md-6 grupo">
                <h4>Empresas do Grupo</h4>
                <ul class="list-unstyled">
                    <li><a href="https://ramalhos.com/br/s" title="Ramalhos" target="_blank"><img src="assets/icons/ramalhos.png" alt="Ramalhos Logo" title="Ramalhos"></a></li>
                    <li><a href="http://soneres.com.br/" title="Soneres" target="_blank"><img src="assets/icons/soneres.png" alt="Soneres Logo" title="Soneres"></a></li>
                    <li><a href="https://www.exporlux.pt/pt/" title="Exporlux" target="_blank"><img src="assets/icons/exporlux.png" alt="Exporlux Logo" title="Exporlux"></a></li>
                </ul>
            </div>
        </div>
    </section>
    <!-- Section Formulário -->
    <section class="form-content">
        <div class="titulo-principal">
            <div class="container">
                <h2>Fale Conosco</h2>
            </div>
        </div>
        <!-- Formulário -->
        <form action="" class="form-faleconosco">
            <div class="container cont-form">
                <div class="row">
                    <div class="col-md-6 input-item">
                        <input type="text" placeholder="Nome" name="nome">
                    </div>
                    <div class="col-md-6 input-item">
                        <input type="email" placeholder="Email" name="email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 input-item">
                        <input type="text" placeholder="Empresa" name="empresa">
                    </div>
                    <div class="col-md-6 input-item">
                        <input type="text" placeholder="Area de Atuação" name="area-atuacao">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 input-item">
                        <input type="text" placeholder="Cidade / Estado" name="cidade-estado">
                    </div>
                    <div class="col-md-6 input-item">
                        <input type="text" placeholder="País" name="pais">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 input-item">
                        <input type="text" placeholder="Assunto" name="assunto">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 input-item">
                        <textarea name="mensagem" placeholder="Mensagem" cols="30" rows="4"></textarea>
                    </div>
                </div>
                <div class="btn-submit">
                    <button type="submit">Enviar <img class="arrow-right" src="assets/icons/arrow-right.svg" alt="Enviar"></button>
                </div>
            </div>
        </form>
    </section>
</main>

<!-- Rodapé -->
<?php require_once('footer.php');?>
