<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <a href="produtos.php">Produtos</a>
            <a href="produtos-area.php">Interior</a>
            <span>Lançamentos</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Lançamentos</h2>
       </div>
    </section>
    <!-- Banner Lançamento -->
    <section class="banner-lancamento container">
        <img src="assets/imgs/lancamento.jpg" alt="Lançamentos">
    </section>
     <!-- Outros produtos -->
     <section class="produtos-categoria">
        <div class="container">
        </div>
        <!-- Lista de produtos -->
        <div class="cont-lista-produtos container">
            <article>
                <a href="produtos-linha.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-1.png" alt="Downlight">
                    </div>
                    <h4>Downlight</h4>
                </a>
            </article>
        </div>
    </section>
</main>
<!-- Rodapé -->
<?php require_once('footer.php');?>
