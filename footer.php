
<!-- Rodapé -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="row">
                    <!-- Links rápidos -->
                    <div class="col-lg-5 links-rapidos">
                        <!-- Lista link rapidos -->
                        <ul class="list-unstyled">
                            <li><a href="produtos.php">Produtos</a></li>
                            <li><a href="projetos.php">Projetos</a></li>
                            <li><a href="novidades.php">Novidades</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 links-rapidos">
                        <!-- Lista link rapidos -->
                        <ul class="list-unstyled">
                            <li><a href="lojas.php">Pontos de Venda</a></li>
                            <li><a href="sobre.php">Sobre</a></li>
                            <li><a href="contato.php">Contato</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- NEWSLETTER -->
            <div class="col-md-5 newsletter">
               <div class="cont-news-letter">
                <h5>Assine e receba novidades</h5>
                    <form action="">
                        <div class="cont-form-news">
                            <i class="fas fa-envelope"></i>
                            <input type="email" placeholder=" Informe o seu email" required>
                            <button type="submit" onclick="window.location.href='agradecimento.php'"><img class="icon-right" src="assets/icons/arrow-white-right.svg" alt="Enviar"></button>
                        </div>
                    </form>
                    <!-- Social links -->
                    <ul class="list-unstyled social-footer">
                        <li><a href="https://instagram.com/light_design_exporlux" target="_blank">Instagram</a></li>
                        <li><a href="https://www.youtube.com/user/LightDesigndoBrasil" target="_blank">Youtube</a></li>
                    </ul>
               </div>
            </div>
        </div>
    </div>
</footer>
<!-- Copyright -->
<div class="copy">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span>Copyright &copy; | Light Design + Exporlux</span>
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled politicas">
                    <li><a href="">Termos de Uso</a></li>
                    <li><a href="">Políticas de Cookies</a></li>
                    <li><a href="oportunidades.php">Oportunidades</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled lang">
                    <li><a href="" class="ativo">Pt</a></li>    
                    <li><a href="" >En</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="mapa-site">
        <!-- Mapa do site -->
        <div class="container mapa-site-content">
        <div class="accordions">
            <button class="accordion-mapa">Mapa do Site <i class="fas fa-chevron-down"></i></button>
            <!-- Conteudo -->
            <div class="panel">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                           <div class="row">
                                <div class="col-lg-6 links-rapidos">
                                    <ul class="list-unstyled">
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="produtos.php">Produtos</a></li>
                                        <li><a href="projetos.php">Projetos</a></li>
                                        <li><a href="novidades.php">Novidades</a></li>
                                        <li><a href="lojas.php">Pontos de Venda</a></li>
                                        <li><a href="sobre.php">Sobre</a></li>
                                        <li><a href="contato.php">Contato</a></li>
                                    </ul>
                                </div>
                           </div>
                        </div>
                        <div class="col-lg-6 links-categorias">
                            <div class="row">
                                <div class="col-lg-4 col-borda">
                                    <ul class="list-unstyled">
                                        <li><h5>Interior</h5></li>
                                        <li><a href="produtos-area.php">Downlight</a></li>
                                        <li><a href="produtos-area.php">Geral</a></li>
                                        <li><a href="produtos-area.php">Spots/Trilhos</a></li>
                                        <li><a href="produtos-area.php">Lineares</a></li>
                                        <li><a href="produtos-area.php">Modulares</a></li>
                                        <li><a href="produtos-area.php">Perfis</a></li>
                                        <li><a href="produtos-area.php">Arandelas</a></li>
                                        <li><a href="produtos-area.php">Pendentes</a></li>
                                        <li><a href="produtos-area.php">Industrial</a></li>
                                        <li><a href="produtos-area.php">Decorativo</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-4">
                                    <ul class="list-unstyled">
                                        <li><h5>Exterior</h5></li>
                                        <li><a href="produtos-area.php">Lineares</a></li>
                                        <li><a href="produtos-area.php">Pendentes</a></li>
                                        <li><a href="produtos-area.php">Balizadores</a></li>
                                        <li><a href="produtos-area.php">Projetores</a></li>
                                        <li><a href="produtos-area.php">Arandelas</a></li>
                                        <li><a href="produtos-area.php">Uplight</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-4">
                                    <ul class="list-unstyled">
                                        <li><a href="produtos-lancamentos.php"><h5>Lançamentos</h5></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>
<script src="assets/js/main.js"></script>


    </body>
</html>