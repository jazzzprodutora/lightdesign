<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="">Home</a>
            <span>404</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Ops... Página não encontrada</h2>
           <p class="mt-5">Verifique se o endereço está corretamente e tente novamente mais tarde</p>
       </div>
    </section>
    <!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>

