<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <span>Agradecimento</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal mt-5 mb-5">
       <div class="container">
           <h2>Obrigado por se cadastrar na nossa newsletter.</h2>
       </div>
    </section>
<!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>
