<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <a href="produtos.php">Produtos</a>
            <a href="produtos-area.php">Interior</a>
            <span>Pendentes</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Downlights</h2>
       </div>
    </section>
    <!-- Chamda da ÁREA -->
    <section class="area-linhas">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                   <a href="produtos-linha.php">
                        <article class="item-linha">
                                <!-- Titulo produto -->
                                <h3>Works</h3>
                                <!-- Linha -->
                                <span>Linha Works</span>
                                <!-- Imagem em destaque -->
                                <div class="img-destaque">
                                    <img src="assets/imgs/produtos/img-linha.png" alt="Works Pend Deep">
                                </div>
                        </article>
                   </a>
                </div>
                <div class="col-lg-4">
                    <a href="produtos-linha.php">
                        <article class="item-linha">
                                <!-- Titulo -->
                                <h3>Jobi</h3>
                                <!-- Linha -->
                                <span>Linha Jobi</span>
                                <!-- Imagem em destaque -->
                                <div class="img-destaque">
                                    <img src="assets/imgs/produtos/img-linha2.png" alt="Magna Retangular">
                                </div>
                        </article>
                    </a>
                </div>
            </div>
        </div>
    </section>
     <!-- Outros produtos -->
     <section class="produtos-categoria">
        <div class="container">
            <!-- Navegação de categorias -->
            <nav class="navegacao-categoria">
                <ul class="list-unstyled">
                    <!-- Dropdown item -->
                    <li>
                        <div class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aplicação <i class="fas fa-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Embutir </a>
                                <a class="dropdown-item" href="#">Sobrepor</a>
                                <a class="dropdown-item" href="#">Pendente</a>
                            </div>
                        </div>
                    </li>
                    <!-- Dropdown item -->
                    <li>
                        <div class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Distribuição de Luz <i class="fas fa-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Difusão </i></a>
                                <a class="dropdown-item" href="#">Destaque </i></a>
                                <a class="dropdown-item" href="#">Indireta </i></a>
                                <a class="dropdown-item" href="#">Difusa/Indireta </i></a>
                            </div>
                        </div>
                    </li>
                    <!-- Dropdown item -->
                    <li>
                        <div class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Linha <i class="fas fa-chevron-down"></i> </i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Works </i></a>
                                <a class="dropdown-item" href="#">Jobi </i></a>
                                <a class="dropdown-item" href="#">Inside </i></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Lista de produtos -->
        <div class="cont-lista-produtos container">
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-1.png" alt="Downlight">
                    </div>
                    <h4>Inside</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-2.png" alt="Geral">
                    </div>
                    <h4>Salmba</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-3.png" alt="Spots/Trilhos">
                    </div>
                    <h4>Outside</h4>
                </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-4.png" alt="Lineares">
                    </div>
                    <h4>Conecta Mini</h4>
                </a>
            </article>
            <article>
               <a href="produto-destaque.php">
                <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-5.png" alt="Modulares">
                    </div>
                    <h4>Circle</h4>
               </a>
            </article>
            <article>
                <a href="produto-destaque.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-6.png" alt="Perfis">
                    </div>
                    <h4>Edro</h4>
                </a>
            </article>          
        </div>
    </section>
</main>
<!-- Rodapé -->
<?php require_once('footer.php');?>
