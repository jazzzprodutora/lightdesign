<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <a href="produtos.php">Produtos</a>
            <span>Exterior</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Exterior</h2>
       </div>
    </section>
    <!-- Outros produtos -->
    <section class="lista-produtos-lancamento">
        <div class="container cont-lista-produtos">
            <article>
               <a href="produtos-categoria.php">
                <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-1.png" alt="Arandelas">
                    </div>
                    <h4>Arandelas</h4>
               </a>
            </article>
            <article>
                <a href="produtos-categoria.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-2.png" alt="Balizadores">
                    </div>
                    <h4>Balizadores</h4>
                </a>
            </article>
            <article>
                <a href="produtos-categoria.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-3.png" alt="Uplights">
                    </div>
                    <h4>Uplights </h4>
                </a>
            </article>
            <article>
               <a href="produtos-categoria.php">
                <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-4.png" alt="Lineares">
                    </div>
                    <h4>Projetores</h4>
               </a>
            </article>
            <article>
                <a href="produtos-categoria.php">
                    <div class="img-destaque">
                        <img src="assets/imgs/produtos/prod-5.png" alt="Modulares">
                    </div>
                    <h4>Lineares/Industriais</h4>
                </a>
            </article>            
        </div>
    </section>
    <!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>

