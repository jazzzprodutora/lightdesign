<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <a href="projetos.php">Projetos</a>
            <span>Smart Fit Santana</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Smart Fit Santana</h2>
       </div>
    </section>
    <!-- Banner Destaque -->
    <section class="banner-destaque">
        <img src="assets/imgs/Smart-Fit-Santana.png" alt="Smart Fit Santana">
    </section>
    <!-- Conteudo -->
    <section class="info-projeto">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-unstyled">
                        <li>Categoria</li>
                        <li>Cidade</li>
                        <li>País</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h3>These days there are tons of photos around. Many are very good. Many just beautiful.</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur eos, quia eligendi tempora ipsam impedit aliquam corrupti omnis facilis. Eveniet commodi ratione alias ipsum consectetur molestias assumenda, accusantium adipisci odit?</p>
                    <ul class="list-unstyled">
                        <li>Equipe</li>
                        <li>Helene Binet - Light Designer</li>
                        <li>Helene Binet - Light Designer</li>
                        <li>Helene Binet - Light Designer</li>
                        <li>Helene Binet - Light Designer</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Vídeo -->
    <section class="video-destaque container">
        <video width="100%" controls>
			<source src="assets/video/video.mp4" type="video/mp4">
		</video>
    </section>
    <!-- Lista de projetos -->
    <section class="lista-projetos">
        <div class="container">
            <div class="row content-projeto">
                <article class="col-md-8 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/bhotel.png" alt="Hotel B">
                        </div>
                    </a>
                </article>
                <article class="col-md-4 item-projeto">
                    <a href="projeto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/Smart-Fit-Santana.png" alt="Smart Fit">
                        </div>
                    </a>
                </article>
            </div>
        </div>
    </section>
    <!-- Produtos Utilizados -->
    <section class="produtos-utilizados">
        <div class="container">
            <h3>Produtos Utilizados</h3>
        </div>
        <div class="owl-carousel wol-theme carrosel-prod-utilizados container">
            <div class="item">
                <article>
                    <a href="produto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-1.png" alt="Nome do produto">
                        </div>
                        <h4>Nome do produto</h4>
                    </a>
                </article>
            </div>
            <div class="item">
                <article>
                    <a href="produto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-3.png" alt="Nome do produto">
                        </div>
                        <h4>Nome do produto</h4>
                    </a>
                </article>
            </div>
            <div class="item">
                <article>
                    <a href="produto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-2.png" alt="Nome do produto">
                        </div>
                        <h4>Nome do produto</h4>
                    </a>
                </article>
            </div>
            <div class="item">
                <article>
                    <a href="produto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-1.png" alt="Nome do produto">
                        </div>
                        <h4>Nome do produto</h4>
                    </a>
                </article>
            </div>
            <div class="item">
                <article>
                    <a href="produto-destaque.php">
                        <div class="img-destaque">
                            <img src="assets/imgs/produtos/prod-3.png" alt="Nome do produto">
                        </div>
                        <h4>Nome do produto</h4>
                    </a>
                </article>
            </div>
        </div>
    </section>
<!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>
