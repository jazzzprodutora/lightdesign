//Menu
let menu = document.querySelector('.header-site .menu-icon');
let linha1 = document.querySelector('#linha1');
let linha2 = document.querySelector('#linha2');
// let linha3 = document.querySelector('#linha3');
let menuConteudo = document.querySelector('.overlay');
menu.addEventListener('click', () => {
  linha1.classList.toggle('efeitoLinha1');
  linha2.classList.toggle('efeitoLinha2');
  // linha3.classList.toggle('efeitoLinha3');
  menuConteudo.classList.toggle('menu-ativo');
});

//Acordion Script (Footer mapa do site)
var acc = document.getElementsByClassName("accordion-mapa");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

//Acordion Script (Lojas)
var acc = document.getElementsByClassName("accordion-lojas");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

//Acordion Script (Menu)
var acc = document.getElementsByClassName("accordion-menu");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

//Acordion Script (Modelo)
var acc = document.getElementsByClassName("accordion-modelo");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

//Carrosel produto destaque
 $('.carrosel-produto-destaque').owlCarousel({
   loop:true,
   autoplay:true,
   margin:15,
   nav:false,
   doots:true,
   responsive:{
      0:{
          items:1
       },
      600:{
          items:1
      },
      1000:{
           items:1
      }
  }
});

//Carrosel produto utilizados
$('.carrosel-prod-utilizados').owlCarousel({
  loop:true,
  autoplay:true,
  margin:15,
  nav:false,
  doots:true,
  responsive:{
     0:{
       items:1
     },
     600:{
       items:2
     },
     1000:{
       items:4
     }
  }
});

//Carrosel Modal
$('.carrosel-modal').owlCarousel({
  loop:true,
  autoplay:true,
  margin:15,
  nav:true,
  doots:true,
  responsive:{
     0:{
         items:1
      },
     600:{
         items:1
     },
     1000:{
          items:1
     }
 }
});


  //Mostrar botão topo fixo home
  document.addEventListener('scroll',()=>{
    const cabecalho = document.querySelector('.scroll-top-home');
    if($(document).scrollTop() > 200){
      cabecalho.classList.add('opacity-scroll');
    }else{
      cabecalho.classList.remove('opacity-scroll');
    }
  });

   //Mostrar botão topo fixo paginas iternas
   document.addEventListener('scroll',()=>{
    const cabecalho = document.querySelector('.scroll-top');
    if($(document).scrollTop() > 1500){
      cabecalho.classList.add('opacity-scroll');
    }else{
      cabecalho.classList.remove('opacity-scroll');
    }
  });
