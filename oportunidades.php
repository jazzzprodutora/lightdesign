<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <span>Oportunidades</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Oportunidades</h2>
       </div>
    </section>
    <!-- Oportunidades -->
    <section class="lista-oportunidades">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-unstyled">
                        <li><h3>Nome da Vaga - Recife / PE</h3></li>
                        <li>Descrição da vaga Lorem ipit. Nihil, eum. Eum, vero impedit aliquid illo magni iste mollitia et officia dolorum quo quaerat nemo ratione, cumque consequatur hic reprehenderit quam.</li>
                        <li>Formação Acadêmica: Ensino superior Completo em Design</li>
                        <li>Quantidade de vagas: 01</li>
                        <li><a href="mailto:oportunidades@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg">Entre em contato</a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-unstyled">
                        <li><h3>Nome da Vaga -  Recife / PE</h3></li>
                        <li>Descrição da vaga Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, eum. Eum, vero impedit aliquid illo magni iste mollitia et officia dolorum quo quaerat nemo ratione, cumque consequatur hic reprehenderit quam.</li>
                        <li>Formação Acadêmica: Ensino superior Completo em Design</li>
                        <li>Quantidade de vagas: 02</li>
                        <li><a href="mailto:oportunidades@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg">Entre em contato</a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-unstyled">
                        <li><h3>Nome da Vaga -  Recife / PE</h3></li>
                        <li>Descrição da vaga Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, eum. Eum, vero impedit aliquid illo magni iste mollitia et officia dolorum quo quaerat nemo ratione, cumque consequatur hic reprehenderit quam.</li>
                        <li>Formação Acadêmica: Ensino superior Completo em Design</li>
                        <li>Quantidade de vagas: 01</li>
                        <li><a href="mailto:oportunidades@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg">Entre em contato</a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-unstyled">
                        <li><h3>Nome da Vaga -  Recife / PE</h3></li>
                        <li>Descrição da vaga Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, eum. Eum, vero impedit aliquid illo magni iste mollitia et officia dolorum quo quaerat nemo ratione, cumque consequatur hic reprehenderit quam.</li>
                        <li>Formação Acadêmica: Ensino superior Completo em Design</li>
                        <li>Quantidade de vagas: 01</li>
                        <li><a href="mailto:oportunidades@lightdesign.com.br"><img class="arrow-right" src="assets/icons/arrow-right.svg">Entre em contato</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>

<!-- Rodapé -->
<?php require_once('footer.php');?>
