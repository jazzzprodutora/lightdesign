<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<?php require_once('scroll.php');?>

<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="">Home</a>
            <span>Novidades</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Novidades</h2>
       </div>
    </section>
    <!-- Menu novidades -->
    <nav class="novidades-nav container">
        <ul class="list-unstyled">
            <li>Todos</li>
            <li>Notícias</li>
            <li>Eventos</li>
            <li>Produtos</li>
        </ul>
    </nav>
    <section class="cont-novidades container">
        <article class="row">
            <!-- Coluna de informações -->
            <div class="col-md-6 info-novidade">
                <!-- data -->
                <div class="data-novidade">
                    <span>22/05/2021</span>
                </div>
                <!-- Titulo novidade -->
                <div class="titulo-novidade">
                    <h3>Proin Eget Tortor Risus Vestibulum Tortor Risus</h3>
                </div>
                <!-- Categoria -->
                <div class="cat">
                    <a href="">Eventos</a>
                </div>
                <div class="content">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam quia reprehenderit recusandae non assumenda! Ipsum error dolore eum impedit eveniet assumenda soluta, expedita accusamus optio similique nesciunt nulla, quaerat quibusdam.</p>
                </div>
                <div class="btn-saiba">
                    <a href="novidade-destaque.php"><img class="arrow-right" src="assets/icons/arrow-right.svg" alt="Saiba mais" title="Saiba mais">  Saiba mais</a>
                </div>
            </div>
            <!-- Coluna Imagem -->
            <div class="col-md-6 img-novidade">
                <a href="novidade-destaque.php"><img src="assets/imgs/novidades/img-novidade.jpg" alt="Titulo da Novidade"></a>
            </div>  
        </article>
        <article class="row">
            <!-- Coluna de informações -->
            <div class="col-md-6 info-novidade">
                <!-- data -->
                <div class="data-novidade">
                    <span>22/05/2021</span>
                </div>
                <!-- Titulo novidade -->
                <div class="titulo-novidade">
                    <h3>Proin Eget Tortor Risus Vestibulum Tortor Risus</h3>
                </div>
                <!-- Categoria -->
                <div class="cat">
                    <a href="">Eventos</a>
                </div>
                <div class="content">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam quia reprehenderit recusandae non assumenda! Ipsum error dolore eum impedit eveniet assumenda soluta, expedita accusamus optio similique nesciunt nulla, quaerat quibusdam.</p>
                </div>
                <div class="btn-saiba">
                    <a href="novidade-destaque.php"><img class="arrow-right" src="assets/icons/arrow-right.svg" alt="Saiba mais" title="Saiba mais">  Saiba mais</a>
                </div>
            </div>
            <!-- Coluna Imagem -->
            <div class="col-md-6 img-novidade">
                <a href="novidade-destaque.php"><img src="assets/imgs/novidades/img-novidade.jpg" alt="Titulo da Novidade"></a>
            </div>  
        </article>
        <article class="row">
            <!-- Coluna de informações -->
            <div class="col-md-6 info-novidade">
                <!-- data -->
                <div class="data-novidade">
                    <span>22/05/2021</span>
                </div>
                <!-- Titulo novidade -->
                <div class="titulo-novidade">
                    <h3>Proin Eget Tortor Risus Vestibulum Tortor Risus</h3>
                </div>
                <!-- Categoria -->
                <div class="cat">
                    <a href="">Eventos</a>
                </div>
                <div class="content">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam quia reprehenderit recusandae non assumenda! Ipsum error dolore eum impedit eveniet assumenda soluta, expedita accusamus optio similique nesciunt nulla, quaerat quibusdam.</p>
                </div>
                <div class="btn-saiba">
                    <a href="novidade-destaque.php"><img class="arrow-right" src="assets/icons/arrow-right.svg" alt="Saiba mais" title="Saiba mais">  Saiba mais</a>
                </div>
            </div>
            <!-- Coluna Imagem -->
            <div class="col-md-6 img-novidade">
                <a href="novidade-destaque.php"><img src="assets/imgs/novidades/img-novidade.jpg" alt="Titulo da Novidade"></a>
            </div>  
        </article>
        <article class="row">
            <!-- Coluna de informações -->
            <div class="col-md-6 info-novidade">
                <!-- data -->
                <div class="data-novidade">
                    <span>22/05/2021</span>
                </div>
                <!-- Titulo novidade -->
                <div class="titulo-novidade">
                    <h3>Proin Eget Tortor Risus Vestibulum Tortor Risus</h3>
                </div>
                <!-- Categoria -->
                <div class="cat">
                    <a href="">Eventos</a>
                </div>
                <div class="content">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam quia reprehenderit recusandae non assumenda! Ipsum error dolore eum impedit eveniet assumenda soluta, expedita accusamus optio similique nesciunt nulla, quaerat quibusdam.</p>
                </div>
                <div class="btn-saiba">
                    <a href="novidade-destaque.php"><img class="arrow-right" src="assets/icons/arrow-right.svg" alt="Saiba mais" title="Saiba mais">  Saiba mais</a>
                </div>
            </div>
            <!-- Coluna Imagem -->
            <div class="col-md-6 img-novidade">
                <a href="novidade-destaque.php"><img src="assets/imgs/novidades/img-novidade.jpg" alt="Titulo da Novidade"></a>
            </div>  
        </article>
    </section>
    <!-- Rodapé -->
    <?php require_once('footer.php');?>
</main>

