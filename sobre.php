<!--Chamada css, js, bootstrap, Menu etc -->
<?php require_once('header.php');?>
<main class="internas">
    <!-- BreadCrumbs (Migalha de pão) -->
    <section class="breadcrumbs">
        <div class="container cont-breadcrumbs">
            <a href="index.php">Home</a>
            <span>Sobre</span>
        </div>
    </section>
    <!-- Titulo Pagina -->
    <section class="titulo-principal">
       <div class="container">
           <h2>Sobre</h2>
       </div>
    </section>
    <!-- Sobre -->
    <section class="info-sobre">
        <div class="container">
            <div class="row">
               <div class="col-md-8">
                    <div class="item-sobre">
                        <h4>1974 </h4>
                        <p>A Light Design é fundada no Rio de Janeiro. Conta com estrutura industrial no Recife, equipe  própria de designers —premiada nacional e internacionalmente— e rede de distribuição em todo  o país. Desenvolve e comercializa luminárias adequadas à cada projeto, além de garantir  assistência técnica permanente. </p>
                    </div>
                   <div class="item-sobre">
                        <h4>1985 </h4>
                        <p>Fundação da Exporlux em Águeda, Portugal. Apostando na utilização de fontes de luz  sustentáveis desde cedo, conta com um Centro Tecnológico LED e arrojados processos de  pesquisa e desenvolvimento. Sua gama de produtos direcionados ao mercado corporativo é  utilizada nos principais projetos em Portugal e no mundo. </p>
                   </div>
                   <div class="item-sobre">
                        <h4>2012 </h4>
                        <p>As empresas dão um grande passo à frente ao realizar sua fusão no Brasil. Atualmente, a Light  Design+Exporlux é parte integrante do Grupo Ramalhos. Não apenas produz no Recife, mas  também fabrica e certifica seus produtos na Europa, com o selo CE de conformidade europeia.  Um diferencial de qualidade sem igual no mercado brasileiro.  </p>
                        <p>A Light Design+Exporlux representa as melhores marcas nacionais e europeias de iluminação  decorativa e é distribuidora da Lutron, líder mundial em controle de iluminação automatizado. Seu  portfólio inclui projetos no Brasil, Portugal, Espanha, Suíça, França e Angola, entre outros.</p>
                   </div>
               </div>
            </div>
        </div>
    </section>
</main>

<!-- Rodapé -->
<?php require_once('footer.php');?>
