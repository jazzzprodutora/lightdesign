<div class="modal fade modal-detalhe" id="moda-destalhes" tabindex="-1" role="dialog" aria-labelledby="moda-destalhesTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <!-- Botão fechar -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    <div class="modal-content">
      <div class="modal-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5">
                    <!-- Titulos -->
                    <div class="titulos">
                        <h3>Works Move</h3>
                        <span>820.0113</span>
                    </div>
                    <!-- Carrosel -->
                    <div id="carrosel-modal" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carrosel-modal" data-slide-to="0" class="active">
                                <img class="d-block w-100" src="assets/imgs/produtos/prod.jpg" alt="" title="">
                            </li>
                            <li data-target="#carrosel-modal" data-slide-to="1">
                                <img class="d-block w-100" src="assets/imgs/produtos/grafico-01.jpg" alt="" title="">
                            </li>
                            <li data-target="#carrosel-modal" data-slide-to="2">
                                <img class="d-block w-100" src="assets/imgs/produtos/grafico-02.jpg" alt="" title="">
                            </li>
                            <li data-target="#carrosel-modal" data-slide-to="3">
                                <img class="d-block w-100" src="assets/imgs/produtos/grafico-03.jpg" alt="" title="">
                            </li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                               <div class="img-destaque">
                                    <img class="d-block w-100" src="assets/imgs/produtos/prod.jpg" alt="" title="">
                               </div>
                            </div>
                            <div class="carousel-item">
                               <div class="img-destaque">
                                    <img class="d-block w-100" src="assets/imgs/produtos/grafico-01.jpg" alt="" title="">
                               </div>
                            </div>
                            <div class="carousel-item">
                               <div class="img-destaque">
                                    <img class="d-block w-100" src="assets/imgs/produtos/grafico-02.jpg" alt="" title="">
                               </div>
                            </div>
                            <div class="carousel-item">
                               <div class="img-destaque">
                                    <img class="d-block w-100" src="assets/imgs/produtos/grafico-03.jpg" alt="" title="">
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Especificações -->
                <div class="col-lg-3 especificacao">
                    <h4>Especificações</h4>
                    <ul class="list-unstyled lista-especificacao">
                        <li>3 editable boards</li>
                        <li>Anonymous board viewers</li>
                        <li>Premade templates</li>
                        <li>3 editable boards</li>
                        <li>Anonymous board viewers</li>
                        <li>Premade templates</li>
                        <li>Premate templates</li>
                        <li>Anonymous board viewers</li>
                        <li>3 editable boards</li>
                        <li>Premade templates</li>
                    </ul>
                    <!-- Lista de celos -->
                    <ul class="list-unstyled selos">
                        <li><img src="assets/icons/selo-1.png" alt=""></li>
                        <li><img src="assets/icons/selo-2.png" alt=""></li>
                        <li><img src="assets/icons/selo-3.png" alt=""></li>
                        <li><img src="assets/icons/selo-4.png" alt=""></li>
                    </ul>
                </div>
                <!-- Downloads -->
                <div class="col-lg-3 downloads">
                    <h4>Downloads</h4>
                    <ul class="list-unstyled">
                        <li>Ficha técnica <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                        <li>Manuais de instalação <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                        <li>Termo de garantia <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                        <li>Blocos CAD <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                        <li>Blocos BIM <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                        <li>Blocos Sketchup <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                        <li>Curvas <a href="" download><img class="download-icon" src="assets/icons/download.svg" alt="download"></a></li>
                    </ul>
                    <!-- Baixar tudo -->
                    <div class="btn-baixar">
                        <a href="" download>Baixar Todos <img class="arrow-right" src="assets/icons/arrow-white-right.svg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>